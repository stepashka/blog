--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.4

-- Started on 2018-07-17 21:18:59

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2833 (class 0 OID 24673)
-- Dependencies: 199
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO public.role (id, name) VALUES (1, 'ADMIN');
INSERT INTO public.role (id, name) VALUES (2, 'USER');


--
-- TOC entry 2831 (class 0 OID 24661)
-- Dependencies: 197
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO public."user" (id, name, surname, email, password, role_id, is_banned) VALUES (7, 'Andrew', 'Zabur', 'zab@mail.ru', '8be4177df4ec5dee8c8bc4f3b49d7a2d', 1, false);
INSERT INTO public."user" (id, name, surname, email, password, role_id, is_banned) VALUES (11, 'Anatolii', 'Shpilka', 'shpilka@mail.ru', '4a7f936ef81eaf0793b35414545e5a75', 2, false);
INSERT INTO public."user" (id, name, surname, email, password, role_id, is_banned) VALUES (14, 'Vitalii', 'Tsurkan', 'tsurkan@gmail.com', '4a7f936ef81eaf0793b35414545e5a75', 2, true);
INSERT INTO public."user" (id, name, surname, email, password, role_id, is_banned) VALUES (12, 'Stepan', 'Oliinyk', 'stepanoliinyk@gmail.com', '4a7f936ef81eaf0793b35414545e5a75', 1, false);
INSERT INTO public."user" (id, name, surname, email, password, role_id, is_banned) VALUES (13, 'Anatolii', 'Skakun', 'skakun@mail.ru', '4a7f936ef81eaf0793b35414545e5a75', 2, true);
INSERT INTO public."user" (id, name, surname, email, password, role_id, is_banned) VALUES (16, 'Dima', 'Havryliuk', 'dima@gmail.com', '4a7f936ef81eaf0793b35414545e5a75', 2, false);
INSERT INTO public."user" (id, name, surname, email, password, role_id, is_banned) VALUES (9, 'Stepan', 'Oliinyk', 'stepanoliinyk09@gmail.com', 'e7d2cf7f94568da27a59a76d90fbc247', 2, false);


--
-- TOC entry 2836 (class 0 OID 24707)
-- Dependencies: 202
-- Data for Name: article; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO public.article (title, image, short_description, content, co_authors, tags, id, author_id, published_date, creation_date, is_published) VALUES ('Trump Nato: Germany''s defence spending attacked', 'img/Trump.jpg', 'US President Donald Trump has suggested Germany''s imports of Russian natural gas are a security concern, as he and other Nato leaders gather for a summit.', 'At talks in Brussels with Nato chief Jens Stoltenberg, he said it was a "very bad thing for Nato" that Germany was "totally controlled by Russia".

He suggested 70% of Germany''s gas imports were Russian but the latest official figure was actually 50.75%.

He has accused Europeans of failing to pay enough for Nato operations.

The Brussels summit comes less than a week before Mr Trump is due to hold his first summit with Vladimir Putin, in Helsinki, reviving concerns among US allies over his proximity to the Russian president.

President Trump shocked some by quipping that the Nato summit might prove harder than next Monday''s summit with Mr Putin.', '{9}', '{3}', 11, 7, '2018-07-11 00:00:00', '2018-07-11 00:00:00', true);
INSERT INTO public.article (title, image, short_description, content, co_authors, tags, id, author_id, published_date, creation_date, is_published) VALUES ('Some title', 'img/black-red-sun-bird.jpg', 'Some description', 'some content', '{9,13}', '{1,2}', 12, 12, '2018-07-12 00:00:00', '2018-07-12 00:00:00', true);
INSERT INTO public.article (title, image, short_description, content, co_authors, tags, id, author_id, published_date, creation_date, is_published) VALUES ('gfdg', '', 'regerg', 'ferferf', '{14,11}', '{1,3}', 16, 7, '2018-07-12 00:00:00', '2018-07-12 00:00:00', false);
INSERT INTO public.article (title, image, short_description, content, co_authors, tags, id, author_id, published_date, creation_date, is_published) VALUES ('Xherdan Shaqiri arrives at Liverpool for medical ahead of Stoke move', 'img/skysports-shaqiri-liverpool_4360828.jpg', 'Liverpool met Shaqiri''s ÃÂÃÂ£13.5m release clause', 'Xherdan Shaqiri has cut short his holiday and has arrived on Merseyside ready for a Liverpool medical, Sky Sports News understands.

Stoke have accepted a bid of just over ÃÂÃÂ£13.5m for the forward, after Liverpool met the release clause valuation in Shaqiri''s contract.

The Swiss midfielder said it was "no secret" he wished to leave Stoke following their relegation from the Premier League.

His last-minute winner against Serbia sent Switzerland through to the World Cup''s round of 16 before their campaign drew to a close with a defeat to Sweden.', '{12}', '{1,3}', 15, 9, '2018-07-16 00:00:00', '2018-07-12 00:00:00', false);
INSERT INTO public.article (title, image, short_description, content, co_authors, tags, id, author_id, published_date, creation_date, is_published) VALUES ('England are in the third-place play-off for the first time since Italia 90', 'img/skysports-gary-lineker-italia-90_4359761.jpg', 'US President Donald Trump has suggested Germany''s imports of Russian natural gas are a security concern, as he and other Nato leaders gather for a summit.', 'England will contest the third-place play-off for the first time since 1990 when they face Belgium on Saturday night. The game will bring back memories of the match against hosts Italy when Sir Bobby RobsonÃ¢s side were beaten 2-1 in Bari.

Chris Waddle''s penalty miss and Paul Gascoigne''s tears are regarded as the sad culmination of England''s brave run at Italia ''90 - penalty shootout defeat to West Germany ending their hopes of winning the World Cup for a second time.

But then, as now, there was another match to play. There were even a number of reasons why it was noteworthy. England goalkeeper Peter Shilton captained the team on his final appearance for his country - winning his 125th cap, still a record for the men''s side.', '{13,14}', '{1}', 14, 7, '2018-07-17 00:00:00', '2018-07-12 00:00:00', true);
INSERT INTO public.article (title, image, short_description, content, co_authors, tags, id, author_id, published_date, creation_date, is_published) VALUES ('Something', '', 'Some description', 'jkhkjhkjkjgkjvgjh', '{}', '{}', 17, 9, '2018-07-12 00:00:00', '2018-07-12 00:00:00', false);
INSERT INTO public.article (title, image, short_description, content, co_authors, tags, id, author_id, published_date, creation_date, is_published) VALUES ('Something', 'img/Trump.jpg', '', '', '{}', '{}', 22, 7, '2018-07-15 00:00:00', '2018-07-15 00:00:00', false);
INSERT INTO public.article (title, image, short_description, content, co_authors, tags, id, author_id, published_date, creation_date, is_published) VALUES ('Man must explore, and this is exploration at its greatest', 'img/post-bg.jpg', 'Problems look mighty small from 150 miles upProblems look mighty small from 150 miles upProblems look mighty small from 150 miles upProblems look mighty small from 150 miles upProblems look mighty small from 150 miles upProblems look mighty small from 150 miles upProblems look mighty small from 150 ', 'Never in all their history have men been able truly to conceive of the world as one: a single sphere, a globe, having the qualities of a globe, a round earth in which all the directions eventually meet, in which there is no center because every point, or none, is center an equal earth which all men occupy as equals. The airman''s earth, if free men make it, will be truly round: a globe in practice, not in theory.

Science cuts two ways, of course; its products can be used for both good and evil. But there''s no turning back from science. The early warnings about technological dangers also come from science.

What was most significant about the lunar voyage was not that man set foot on the Moon but that they set eye on the earth.

A Chinese tale tells of some men sent to harm a young girl who, upon seeing her beauty, become her protectors rather than her violators. That''s how I felt seeing the Earth for the first time. I could not help but love and cherish her.

For those who have seen the Earth from space, and for the hundreds and perhaps thousands more who will, the experience most certainly changes your perspective. The things that we share in our world are far more valuable than those which divide us.', '{}', '{}', 7, 7, '2018-07-16 00:00:00', '2018-07-04 00:00:00', true);
INSERT INTO public.article (title, image, short_description, content, co_authors, tags, id, author_id, published_date, creation_date, is_published) VALUES ('Science has not yet mastered prophecy', 'img/skysports-gary-lineker-italia-90_4359761.jpg', 'We predict too much for the next year and yet far too little for the next ten.We predict too much for the next year and yet far too little for the next ten.We predict too much for the next year and yet far too little for the next ten.We predict too much for the next year and yet far too little for the next ten.We predict too much for the next year and yet far too little for the next ten.We predict too much for the next year and yet far too little for the next ten.We predict too much for the next', 'Never in all their history have men been able truly to conceive of the world as one: a single sphere, a globe, having the qualities of a globe, a round earth in which all the directions eventually meet, in which there is no center because every point, or none, is center an equal earth which all men occupy as equals. The airman''s earth, if free men make it, will be truly round: a globe in practice, not in theory.

Science cuts two ways, of course; its products can be used for both good and evil. But there''s no turning back from science. The early warnings about technological dangers also come from science.

What was most significant about the lunar voyage was not that man set foot on the Moon but that they set eye on the earth.

A Chinese tale tells of some men sent to harm a young girl who, upon seeing her beauty, become her protectors rather than her violators. That''s how I felt seeing the Earth for the first time. I could not help but love and cherish her.

For those who have seen the Earth from space, and for the hundreds and perhaps thousands more who will, the experience most certainly changes your perspective. The things that we share in our world are far more valuable than those which divide us.', '{7}', '{3}', 8, 9, '2018-07-17 00:00:00', '2018-07-04 00:00:00', true);
INSERT INTO public.article (title, image, short_description, content, co_authors, tags, id, author_id, published_date, creation_date, is_published) VALUES ('England are in the third-place play-off for the first time since Italia 90', 'img/skysports-gary-lineker-italia-90_4359761.jpg', 'England lost 2-1 to Italy at the 1990 World Cup', 'England will contest the third-place play-off for the first time since 1990 when they face Belgium on Saturday night. The game will bring back memories of the match against hosts Italy when Sir Bobby side were beaten 2-1 in Bari.

Chris Waddle''s penalty miss and Paul Gascoigne''s tears are regarded as the sad culmination of England''s brave run at Italia ''90 - penalty shootout defeat to West Germany ending their hopes of winning the World Cup for a second time.

But then, as now, there was another match to play. There were even a number of reasons why it was noteworthy. England goalkeeper Peter Shilton captained the team on his final appearance for his country - winning his 125th cap, still a record for the men''s side.', '{11,13}', '{1}', 13, 7, '2018-07-17 00:00:00', '2018-07-12 00:00:00', true);


--
-- TOC entry 2835 (class 0 OID 24700)
-- Dependencies: 201
-- Data for Name: tag; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO public.tag (id, name) VALUES (1, 'Sport');
INSERT INTO public.tag (id, name) VALUES (2, 'Architecture');
INSERT INTO public.tag (id, name) VALUES (3, 'news');


--
-- TOC entry 2843 (class 0 OID 0)
-- Dependencies: 203
-- Name: article_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.article_id_seq', 29, true);


--
-- TOC entry 2844 (class 0 OID 0)
-- Dependencies: 198
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.role_id_seq', 2, true);


--
-- TOC entry 2845 (class 0 OID 0)
-- Dependencies: 200
-- Name: tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.tag_id_seq', 3, true);


--
-- TOC entry 2846 (class 0 OID 0)
-- Dependencies: 196
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.user_id_seq', 16, true);


-- Completed on 2018-07-17 21:19:00

--
-- PostgreSQL database dump complete
--

