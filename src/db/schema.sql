--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.4

-- Started on 2018-07-17 21:19:45

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12924)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2837 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 202 (class 1259 OID 24707)
-- Name: article; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.article (
    title character varying(255) NOT NULL,
    image character varying(255),
    short_description text,
    content text,
    co_authors integer[],
    tags integer[],
    id integer NOT NULL,
    author_id integer,
    published_date timestamp without time zone NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    is_published boolean NOT NULL
);


ALTER TABLE public.article OWNER TO root;

--
-- TOC entry 203 (class 1259 OID 24713)
-- Name: article_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.article_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.article_id_seq OWNER TO root;

--
-- TOC entry 2838 (class 0 OID 0)
-- Dependencies: 203
-- Name: article_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.article_id_seq OWNED BY public.article.id;


--
-- TOC entry 199 (class 1259 OID 24673)
-- Name: role; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.role (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.role OWNER TO root;

--
-- TOC entry 198 (class 1259 OID 24671)
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO root;

--
-- TOC entry 2839 (class 0 OID 0)
-- Dependencies: 198
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.role_id_seq OWNED BY public.role.id;


--
-- TOC entry 201 (class 1259 OID 24700)
-- Name: tag; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.tag (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.tag OWNER TO root;

--
-- TOC entry 200 (class 1259 OID 24698)
-- Name: tag_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.tag_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tag_id_seq OWNER TO root;

--
-- TOC entry 2840 (class 0 OID 0)
-- Dependencies: 200
-- Name: tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.tag_id_seq OWNED BY public.tag.id;


--
-- TOC entry 197 (class 1259 OID 24661)
-- Name: user; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    surname character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    role_id integer,
    is_banned boolean NOT NULL
);


ALTER TABLE public."user" OWNER TO root;

--
-- TOC entry 196 (class 1259 OID 24659)
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO root;

--
-- TOC entry 2841 (class 0 OID 0)
-- Dependencies: 196
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- TOC entry 2693 (class 2604 OID 24715)
-- Name: article id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.article ALTER COLUMN id SET DEFAULT nextval('public.article_id_seq'::regclass);


--
-- TOC entry 2691 (class 2604 OID 24676)
-- Name: role id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public.role_id_seq'::regclass);


--
-- TOC entry 2692 (class 2604 OID 24703)
-- Name: tag id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tag ALTER COLUMN id SET DEFAULT nextval('public.tag_id_seq'::regclass);


--
-- TOC entry 2690 (class 2604 OID 24664)
-- Name: user id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- TOC entry 2705 (class 2606 OID 24729)
-- Name: article article_id_pk; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.article
    ADD CONSTRAINT article_id_pk PRIMARY KEY (id);


--
-- TOC entry 2700 (class 2606 OID 24678)
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- TOC entry 2703 (class 2606 OID 24705)
-- Name: tag tag_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (id);


--
-- TOC entry 2696 (class 2606 OID 24666)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 2706 (class 1259 OID 24722)
-- Name: article_id_uindex; Type: INDEX; Schema: public; Owner: root
--

CREATE UNIQUE INDEX article_id_uindex ON public.article USING btree (id);


--
-- TOC entry 2697 (class 1259 OID 24679)
-- Name: role_id_uindex; Type: INDEX; Schema: public; Owner: root
--

CREATE UNIQUE INDEX role_id_uindex ON public.role USING btree (id);


--
-- TOC entry 2698 (class 1259 OID 24691)
-- Name: role_name_uindex; Type: INDEX; Schema: public; Owner: root
--

CREATE UNIQUE INDEX role_name_uindex ON public.role USING btree (name);


--
-- TOC entry 2701 (class 1259 OID 24706)
-- Name: tag_id_uindex; Type: INDEX; Schema: public; Owner: root
--

CREATE UNIQUE INDEX tag_id_uindex ON public.tag USING btree (id);


--
-- TOC entry 2694 (class 1259 OID 24667)
-- Name: user_id_uindex; Type: INDEX; Schema: public; Owner: root
--

CREATE UNIQUE INDEX user_id_uindex ON public."user" USING btree (id);


--
-- TOC entry 2708 (class 2606 OID 24723)
-- Name: article fk_author; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.article
    ADD CONSTRAINT fk_author FOREIGN KEY (author_id) REFERENCES public."user"(id) ON DELETE CASCADE;


--
-- TOC entry 2707 (class 2606 OID 24686)
-- Name: user fk_role; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT fk_role FOREIGN KEY (role_id) REFERENCES public.role(id) ON DELETE CASCADE;


-- Completed on 2018-07-17 21:19:45

--
-- PostgreSQL database dump complete
--

