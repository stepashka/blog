<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: stepan
  Date: 03.07.2018
  Time: 12:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="layouts/navbar.jsp"/>
<jsp:include page="layouts/_header.jsp"/>
<c:if test="${empty sessionScope.authorizedUser}">
<div class="container offset-4" id="wrap">
    <div class="row">
        <div class="col-md-6 col-md-offset-4">
            <form action="registration" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                    <c:if test="${not empty errors}">
                        <div class="alert alert-danger" role="alert">
                            <c:forEach items="${errors}" var="error">
                                ${error}<br>
                            </c:forEach>
                        </div>
                    </c:if>
                <legend>Sign Up</legend>
                <div class="row">
                    <div class="col-xs-6 col-md-6">
                        <input type="text" name="name" class="form-control input-lg" placeholder="name"  />
                    </div>
                    <div class="col-xs-6 col-md-6">
                        <input type="text" name="surname" class="form-control input-lg" placeholder="Surname"  />
                    </div>
                </div>
                <br>
                <input type="text" name="email" class="form-control input-lg" placeholder="Your Email" />
                <br>
                <input type="password" name="password" class="form-control input-lg" placeholder="Password"  />
                <br>
                <input type="password" name="repeatedPassword" class="form-control input-lg" placeholder="Confirm Password"  />
                <br />
                <button class="btn btn-lg btn-primary btn-block signup-btn" type="submit">
                    Create account
                </button>
            </form>
        </div>
    </div>
</div>
</c:if>
<c:if test="${not empty sessionScope.authorizedUser}">
    <span class="subheading">user already signed in</span>
</c:if>
<jsp:include page="layouts/_footer.jsp"/>