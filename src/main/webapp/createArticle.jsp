<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: stepan
  Date: 09/07/2018
  Time: 13:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="layouts/navbar.jsp"/>
<jsp:include page="layouts/_header.jsp"/>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <form method="post" action="createArticle" enctype="multipart/form-data">
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls">
                            <label>Title*</label>
                            <input type="text" class="form-control" placeholder="Title*" id="name" name="title" required data-validation-required-message="Please enter your title.">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls">
                            <label>Short description</label>
                            <input type="text" class="form-control" maxlength="500" placeholder="Short description" name="shortDescription" id="email" data-validation-required-message="Please enter your short description.">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls">
                            <label>Content</label>
                            <textarea rows="5" class="form-control" placeholder="Content" name="content" id="message"  data-validation-required-message="Please enter a message."></textarea>
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="control-group">
                        <span class="subheading">Tags</span> <br>
                        <select class="custom-select" name="chooseTags" id="chooseTags" multiple="multiple">
                            <c:forEach items="${tags}" var="tag">
                                <option value="${tag.getId()}">${tag.getName()}</option>
                            </c:forEach>
                        </select>
                        <p></p>
                    </div>
                    <br>
                    <div class="control-group">
                        <span class="subheading">Co-authors</span> <br>
                        <select class="custom-select" name="chooseCoAuthors" id="chooseCoAuthors" multiple="multiple">
                            <c:forEach items="${users}" var="user">
                                <option value="${user.getId()}">${user.getEmail()}</option>
                            </c:forEach>
                        </select>
                        <p></p>
                    </div>
                    <p></p>
                    <div class="control-group">
                            <input type="file" accept="image/*" name="file">
                    </div>
                    <p></p>
                    <div class="control-group">
                            <input type="checkbox" name="publish"> Publish
                    </div>
                    <p></p>
                    <div id="success"></div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-size" name="" id="sendMessageButton">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<jsp:include page="layouts/_footer.jsp"/>