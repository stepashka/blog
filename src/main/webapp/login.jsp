<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: stepan
  Date: 05.07.2018
  Time: 12:14
  To change this template use File | Settings | File Templates.
--%>
<jsp:include page="layouts/navbar.jsp"/>
    <header class="masthead" style="background-image: url('img/home-bg.jpg');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="site-heading">
                        <c:if test="${empty sessionScope.authorizedUser}">
                            <div class="container">
                                <form class="form-horizontal" role="form" method="POST" action="login">
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <h2>Please Login</h2>
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <div class="form-group has-danger">
                                                <label class="sr-only" for="email">E-Mail Address</label>
                                                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                                    <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-at"></i></div>
                                                    <input type="email" name="email" class="form-control" id="email"
                                                           placeholder="you@example.com" required autofocus>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="sr-only" for="password">Password</label>
                                                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                                    <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-key"></i></div>
                                                    <input type="password" name="password" class="form-control" id="password"
                                                           placeholder="Password" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-control-feedback">
                           <span class="text-danger align-middle">
                               <c:if test="${not empty errorMessage}">
                                   ${errorMessage}
                               </c:if>
                           </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-top: 1rem">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-success btn-size" ><i class="fa fa-sign-in"></i> Sign in</button>
                                        </div>
                                    </div>
                                    <span><a href="registration" style="padding-bottom: .15rem">Not registered yet?</a></span>
                                </form>
                            </div>
                        </c:if>
                        <c:if test="${not empty sessionScope.authorizedUser}">
                            <span class="subheading">user already signed in</span>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </header>