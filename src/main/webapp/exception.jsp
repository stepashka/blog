<%--
  Created by IntelliJ IDEA.
  User: stepan
  Date: 17/07/2018
  Time: 11:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="layouts/navbar.jsp"/>
<jsp:include page="layouts/_header.jsp"/>
<div class="container text-center">
    <div class="alert alert-danger" role="alert">
        Something went wrong!
    </div>
</div>
<jsp:include page="layouts/_footer.jsp"/>
