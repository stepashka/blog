<%--
  Created by IntelliJ IDEA.
  User: stepan
  Date: 14/07/2018
  Time: 20:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../layouts/navbar.jsp"/>
<jsp:include page="../layouts/_header.jsp"/>
<div class="container text-center">
    <div class="alert alert-danger" role="alert">
        You have no enough rights!
    </div>
</div>
<jsp:include page="../layouts/_footer.jsp"/>
