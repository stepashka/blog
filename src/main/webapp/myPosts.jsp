<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: stepan
  Date: 16/07/2018
  Time: 13:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="layouts/navbar.jsp"/>
<jsp:include page="layouts/_header.jsp"/>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <c:forEach items="${articleList}" var="article">
                        <c:if test="${article.isPublished()}">
                            <div class="post-preview">
                                <a href="article?id=${article.getId()}">
                                    <h2 class="post-title">
                                            ${article.getTitle()}
                                    </h2>
                                    <h3 class="post-subtitle">
                                            ${article.getShortDescription()}
                                    </h3>
                                </a>
                                <p class="post-meta">Posted by
                                    <a href="?id=${article.getAuthor().getId()}">${article.getAuthor().getName()} ${article.getAuthor().getSurname()}</a>
                                    on ${String.valueOf(article.getCreatedDate().getMonth()).toLowerCase()}, ${article.getCreatedDate().getDayOfMonth()},  ${article.getCreatedDate().getYear()}
                                    <c:if test="${!article.getCreatedDate().equals(article.getPublishedDate())}">
                                        <br>Edited on ${String.valueOf(article.getPublishedDate().getMonth()).toLowerCase()}, ${article.getPublishedDate().getDayOfMonth()},  ${article.getPublishedDate().getYear()}
                                    </c:if>
                                </p>
                            </div>
                            <hr>
                        </c:if>
                    </c:forEach>
                </div>
            </div>
        </div>
<jsp:include page="layouts/_footer.jsp"/>
