<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: stepan
  Date: 04.07.2018
  Time: 13:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="layouts/navbar.jsp"/>
<header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="site-heading">
                    <h1>User list</h1>
                    <span class="subheading">A Blog Theme by Start Bootstrap</span>
                </div>
            </div>
        </div>
    </div>
</header>
    <table class="table table-striped">
        <thead>
            <tr>
                <td>Surname</td>
                <td>Name</td>
                <td>Email</td>
                <td>Role</td>
                <td></td>
                <td></td>
            </tr>
        </thead>
        <tbody>
        <c:forEach items="${userList}" var="user">
            <tr>
                <td>${user.getSurname()}</td>
                <td>${user.getName()}</td>
                <td>${user.getEmail()}</td>
                <td>${user.getRole().getName()}</td>
                <td>
                    <button type="button" class="btn btn-success usr-btn-size" data-toggle="modal" data-target="#setRoleModal${user.getId()}">
                        Edit
                    </button>
                </td>
                <c:choose>
                    <c:when test="${user.isBanned()}">
                        <td>
                            <button type="button" class="btn btn-info usr-btn-size" data-toggle="modal" data-target="#isBannedModal${user.getId()}">
                                UnBan
                            </button>
                        </td>
                    </c:when>
                    <c:otherwise>
                        <td>
                            <button type="button" class="btn btn-danger usr-btn-size" data-toggle="modal" data-target="#isBannedModal${user.getId()}">
                                Ban
                            </button>
                        </td>
                    </c:otherwise>
                </c:choose>
            </tr>
            <div class="modal fade" id="isBannedModal${user.getId()}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Confirm action</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <c:choose>
                                <c:when test="${user.isBanned()}">
                                    Are you sure you want to unBan the user?
                                </c:when>
                                <c:otherwise>
                                    Are you sure you want to banned the user?
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                <a href="?id=${user.getId()}&isBanned=${!user.isBanned()}" class="btn btn-primary">Yes</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="setRoleModal${user.getId()}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabell">Changing user role</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="userList?uid=${user.getId()}" method="post">
                            <div class="modal-body">
                                <select name="roleId" class="custom-select">
                                    <c:forEach items="${roles}" var="role">
                                        <c:choose>
                                            <c:when test="${user.getRole().getId() == role.getId()}">
                                                <option value="${role.getId()}" selected>${role.getName()}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${role.getId()}">${role.getName()}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" href="?uid=${user.getId()}" class="btn btn-primary">Yes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </c:forEach>
        </tbody>
    </table>
<jsp:include page="layouts/_footer.jsp"/>