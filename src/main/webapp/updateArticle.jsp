<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Created by IntelliJ IDEA.
  User: stepan
  Date: 10/07/2018
  Time: 14:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="layouts/navbar.jsp"/>
<jsp:include page="layouts/_header.jsp"/>
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
            <form method="post" action="updateArticle" enctype="multipart/form-data">
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label>Title*</label>
                        <input type="text" class="form-control" placeholder="Title*" id="name" name="title" value="${editingArticle.getTitle()}" required data-validation-required-message="Please enter your title.">
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label>Short description</label>
                        <input type="text" class="form-control" maxlength="500" placeholder="Short description" name="shortDescription" id="email" value="${editingArticle.getShortDescription()}"  data-validation-required-message="Please enter your short description.">
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label>Content</label>
                        <textarea rows="5" class="form-control" placeholder="Content" name="content" id="message" data-validation-required-message="Please enter a content.">${editingArticle.getContent()}</textarea>
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <span class="subheading">Tags</span> <br>
                    <select class="custom-select" name="chooseTags" id="chooseTags" multiple="multiple">
                        <c:forEach items="${tags}" var="tag">
                            <c:choose>
                                <c:when test="${fn:contains(editingArticle.getTags(), tag)}">
                                    <option value="${tag.getId()}" selected>${tag.getName()}</option>
                                </c:when>
                                <c:otherwise>
                                <option value="${tag.getId()}">${tag.getName()}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <p></p>
                </div>
                <div class="control-group">
                    <span class="subheading">co-authors</span> <br>
                    <select class="custom-select" name="chooseCoAuthors" id="chooseCoAuthors" multiple="multiple">
                        <c:forEach items="${users}" var="user">
                            <c:choose>
                                <c:when test="${fn:contains(editingArticle.getCoAuthors(), user)}">
                                    <option selected value="${user.getId()}">${user.getEmail()}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${user.getId()}">${user.getEmail()}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <p></p>
                </div>
                <div class="control-group">
                    <span class="subheading">current image</span> <br>
                        <img style="width: 500px; height: 300px" src="${editingArticle.getImage()}">
                </div>
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls"><br>
                        <input type="file" accept="image/*" name="file">
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <input type="checkbox" name="publish" value="${editingArticle.isPublished()}"> Publish
                    </div>
                </div>
                <br>
                <div id="success"></div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary" name="" id="sendMessageButton">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<jsp:include page="layouts/_footer.jsp"/>
