<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Created by IntelliJ IDEA.
  User: stepan
  Date: 05/07/2018
  Time: 21:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="layouts/navbar.jsp"/>
<!-- Page Header -->
<header class="masthead" style="background-image: url('${article.getImage()}')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="post-heading">
                    <h1>${article.getTitle()}</h1>
                    <h2 class="subheading">${article.getShortDescription()}</h2>
                    <span class="meta">Posted by
                <a href="articlesByAuthor?id=${article.getAuthor().getId()}">${article.getAuthor().getName()} ${article.getAuthor().getSurname()}</a>
                on ${article.getCreatedDate().getMonth()}, ${article.getCreatedDate().getDayOfMonth()},  ${article.getCreatedDate().getYear()}</</span>
                    <p></p>
                    <c:if test="${not empty article.getCoAuthors()}">
                        <span class="meta">co-authors:
                        <c:forEach items="${article.getCoAuthors()}" var="coAuthor">
                            <a href="home?id=${coAuthor.getId()}">${coAuthor.getName()} ${coAuthor.getSurname()}</a>,
                        </c:forEach>
                    </span>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Post Content -->
<article>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <p>${article.getContent()}</p>

                <c:if test="${not empty article.getTags()}">
                <p>Tags:
                    <c:forEach items="${article.getTags()}" var="tag">
                    <a href="articlesByTag?id=${tag.getId()}">${tag.getName()}, </a>
                    </c:forEach>
                    </c:if>
                    <br>
                    <c:if test="${article.getAuthor().getEmail().equals(authorizedUser) || isCoAuthor}">
                    <a href="updateArticle?id=${article.getId()}" class="btn-size btn btn-success">edit</a>
                    </c:if>
                    <c:if test="${article.getAuthor().getEmail().equals(authorizedUser)}">
                    <button type="button" class="btn-size btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                        delete
                    </button>
                    </c:if>
                    <c:choose>
                        <c:when test="${article.isPublished()}">
                            <c:if test="${isAdmin || article.getAuthor().getEmail().equals(authorizedUser) || isCoAuthor}">
                            <a href="publish?id=${article.getId()}&isPublished=false" class="btn-size btn btn-info">unPublish</a>
                            </c:if>
                        </c:when>
                        <c:otherwise>
                            <c:if test="${isAdmin || article.getAuthor().getEmail().equals(authorizedUser) || isCoAuthor}">
                            <a href="publish?id=${article.getId()}&isPublished=true" class="btn btn-primary btn-size">publish</a>
                            </c:if>
                        </c:otherwise>
                    </c:choose>
            </div>
        </div>
    </div>
</article>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Deleting article</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Are you sure?
            </div>
            <div class="modal-footer">
                <a href="delete?id=${article.getId()}" class="btn btn-danger">Yes</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<jsp:include page="layouts/_footer.jsp"/>