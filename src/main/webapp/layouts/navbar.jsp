<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: stepan
  Date: 05/07/2018
  Time: 22:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Clean Blog</title>
    <link href="./vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/custom.css" rel="stylesheet">
    <link href="./vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template -->
    <link href="./vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href="./css/clean-blog.min.css" rel="stylesheet">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="./node_modules/bootstrap-multiselect/dist/css/bootstrap-multiselect.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand" href="userInfo">
            Hello,
            <c:if test="${not empty sessionScope.authorizedUser}">
                ${authorizedUserName}
            </c:if>
            <c:if test="${empty sessionScope.authorizedUser}">
                friend
            </c:if>!
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="home">Home</a>
                </li>
                <c:if test="${isAdmin}">
                    <li class="nav-item">
                        <a class="nav-link" href="userList">User list</a>
                    </li>
                </c:if>
                <c:if test="${not empty sessionScope.authorizedUser}">
                    <li class="nav-item">
                        <a class="nav-link" href="createArticle">add post</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="myPosts">My Posts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="myCoAuthorPosts">co-author Posts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="drafts">Drafts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="logout">Logout</a>
                    </li>
                </c:if>
                <c:if test="${empty sessionScope.authorizedUser}">
                <li class="nav-item">
                    <a class="nav-link" href="login">Sign in</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="registration">Sign up</a>
                </li>
                </c:if>
            </ul>
        </div>
    </div>
</nav>