package filter;

import model.Article;
import model.User;
import org.apache.log4j.Logger;
import service.ArticleService;
import service.UserService;
import servlet.ArticleServlet;
import utils.MyUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebFilter(urlPatterns = {"/updateArticle"})
public class UpdateArticleFilter implements Filter {

    private final static Logger LOGGER = Logger.getLogger(ArticleServlet.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session = request.getSession();
        ArticleService articleService = new ArticleService();
        UserService userService = new UserService();
        try {
            if (request.getParameter("id") != null) {
                Article article = articleService.getArticleById(Integer.parseInt(request.getParameter("id")));
                User user = userService.getByEmail(MyUtils.getAuthorizedUserEmail(session));

                if (article.getAuthor().equals(user) || article.getCoAuthors().contains(user)) {
                    filterChain.doFilter(servletRequest, servletResponse);
                } else {
                    RequestDispatcher dispatcher = request.getRequestDispatcher("message/message.jsp");
                    dispatcher.forward(servletRequest, servletResponse);
                }
            } else {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error("Something wrong", e);
        }
    }

    @Override
    public void destroy() {

    }
}
