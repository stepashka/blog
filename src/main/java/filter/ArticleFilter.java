package filter;

import model.Article;
import org.apache.log4j.Logger;
import service.ArticleService;
import service.UserService;
import servlet.ArticleServlet;
import utils.MyUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;

@WebFilter(urlPatterns = {"/article"})
public class ArticleFilter implements Filter {

    private final static Logger LOGGER = Logger.getLogger(ArticleServlet.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        ArticleService articleService = new ArticleService();
        UserService userService = new UserService();
        try {
            Article article = articleService.getArticleById(Integer.parseInt(request.getParameter("id")));
            if (!article.isPublished() && !article.getCoAuthors().contains(userService.getByEmail(MyUtils.getAuthorizedUserEmail(request.getSession()))) &&
                    (!article.getAuthor().getEmail().equals(MyUtils.getAuthorizedUserEmail(request.getSession()))
                            || MyUtils.getAuthorizedUserEmail(request.getSession()) == null)) {
                RequestDispatcher dispatcher = request.getRequestDispatcher("/message/message.jsp");
                dispatcher.forward(servletRequest, servletResponse);
            } else {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error("Something wrong", e);
        }
    }

    @Override
    public void destroy() {

    }
}
