package filter;

import org.apache.log4j.Logger;
import service.UserService;
import servlet.ArticleServlet;
import utils.MyUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebFilter(urlPatterns = {"/userList", })
public class AdminFilter implements Filter {

    private final static Logger LOGGER = Logger.getLogger(ArticleServlet.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        UserService userService = new UserService();
        HttpSession session = request.getSession();

        if (MyUtils.getAuthorizedUserEmail(session) != null) {
            try {
                if (!userService.isAdmin(userService.getByEmail(MyUtils.getAuthorizedUserEmail(session)))) {
                    RequestDispatcher dispatcher = request.getRequestDispatcher("/home");
                    dispatcher.forward(servletRequest, servletResponse);
                }
            } catch (SQLException | ClassNotFoundException e) {
                LOGGER.error("Something wrong", e);
            }
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
