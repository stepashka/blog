package filter;

import model.Article;
import org.apache.log4j.Logger;
import service.ArticleService;
import servlet.ArticleServlet;
import utils.MyUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;

@WebFilter(urlPatterns = {"/delete"})
public class DeleteArticleFilter implements Filter {

    private final static Logger LOGGER = Logger.getLogger(ArticleServlet.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        ArticleService articleService = new ArticleService();
        Article article = null;
        try {
            article = articleService.getArticleById(Integer.parseInt(request.getParameter("id")));
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error("Something wrong", e);
        }
        if (article.getAuthor().getEmail().equals(MyUtils.getAuthorizedUserEmail(request.getSession()))) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/message/message.jsp");
            dispatcher.forward(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
