package service;

import dao.impl.ArticleDAO;
import dao.impl.TagDAO;
import dao.impl.UserDAO;
import dao.interfaces.IArticleDAO;
import model.Article;
import model.Tag;
import model.User;

import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class ArticleService {

    private IArticleDAO dao;

    public ArticleService() {
        dao = ArticleDAO.getArticleDAO();
    }

    public List<Article> getAllArticles() throws SQLException, ClassNotFoundException {
        return dao.getAllArticles();
    }

    public Article getArticleById(int id) throws SQLException, ClassNotFoundException {
        return dao.getById(id);
    }

    public List<Article> getArticlesByAuthorId(int authorId) throws SQLException, ClassNotFoundException {
        return dao.getArticlesByAuthorId(authorId);    }

    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length()-1);
            }
        }
        return "";
    }

    public Article createArticle(Article article) throws SQLException, ClassNotFoundException {
        return dao.add(article);
    }

    public void updateArticle(Article article) throws SQLException, ClassNotFoundException {
        dao.edit(article);
    }

    public List<User> getCoAuthorsByIntegerList(List<Integer> coAuthors) throws SQLException, ClassNotFoundException {
        return dao.getCoAuthorsByIntegerList(coAuthors);
    }

    public List<Tag> getTagsByIntegerList(List<Integer> integerTags) throws SQLException, ClassNotFoundException {
        return dao.getTagsByIntegerList(integerTags);
    }

    public List<Article> getArticlesByCoAuthorId(int id) throws SQLException, ClassNotFoundException {
        return dao.getArticlesByCoAuthorId(id);
    }

    public List<Article> getArticleByTagId(int id) throws SQLException, ClassNotFoundException {
        return dao.getArticlesByTagId(id);
    }

    public void deleteArticle(int id) throws SQLException, ClassNotFoundException {
        dao.delete(getArticleById(id));
    }

    public String getImagePath(Part part, String applicationPath) throws IOException {
        if (part.getSize() != 0) {
            String savePath = applicationPath + "/img";
            File fileSaveDir = new File(savePath);
            if (fileSaveDir.exists()) {
                fileSaveDir.mkdir();
            }
            String fileName = extractFileName(part);
            part.write(savePath + "/" + fileName);
            return "img/" + fileName;
        } else {
            return null;
        }
    }

    public List<Article> getNotPublishedArticlesByUserId(int userId) throws SQLException, ClassNotFoundException {
        return dao.getNotPublishedArticlesByUserId(userId);
    }
}
