package service;

import dao.impl.UserDAO;
import model.User;
import utils.Security;

import javax.validation.Configuration;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserValidator {

    private User user;
    private String repeatedPassword;

    private UserValidator() {}

    public UserValidator(User user, String repeatedPassword) {
        this.user = user;
        this.repeatedPassword = repeatedPassword;
    }

    public List<String> getErrors()  {
        List<String> list = new ArrayList<>();
        Configuration<?> configuration = Validation.byDefaultProvider().configure();
        ValidatorFactory factory = configuration.buildValidatorFactory();
        javax.validation.Validator validator = factory.getValidator();
        factory.close();
        try {
            if (isEmailInDB(user.getEmail())) {
                list.add("User with email like that already exists");
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        for (ConstraintViolation<User> violation : validator.validate(user)) {
            String error = violation.getPropertyPath() + " " + violation.getMessage();
            error = error.substring(0, 1).toUpperCase() + error.substring(1);
            list.add(error);
        }
        if (!isPasswordMatch(user.getPassword(), repeatedPassword)) {
            list.add("Password must match");
        }
        return list;
    }

    private boolean isEmailInDB(String email) throws SQLException, ClassNotFoundException {
        UserDAO dao = UserDAO.getDao();
        return dao.getByEmail(email) != null;
    }

    private boolean isPasswordMatch(String password, String repeatedPassword) {
        return password.equals(repeatedPassword);
    }
}
