package service;

import dao.interfaces.IUserDAO;
import utils.Security;
import dao.impl.UserDAO;
import model.User;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;

public class UserService {

    private IUserDAO dao;

    public UserService() {
        dao = UserDAO.getDao();
    }

    public List<User> getAllUsers() throws SQLException, ClassNotFoundException {
        return dao.getAllUsers();
    }

    public boolean checkUser(String email, String password) throws UnsupportedEncodingException, NoSuchAlgorithmException, SQLException, ClassNotFoundException {
        User user = null;
        if (email != null && !email.equals("") && password != null && !password.equals("")) {
            password = Security.md5(Security.md5(Security.md5(password)));
            user = dao.getByEmail(email);
            if (user != null) {
                if (!password.equals(user.getPassword())) user = null;
            }
        }
        return user != null;
    }

    public void writeToDB(User user) throws SQLException, ClassNotFoundException, UnsupportedEncodingException, NoSuchAlgorithmException {
        user.setPassword(Security.md5(Security.md5(Security.md5(user.getPassword()))));
        dao.add(user);
    }

    public List<String> getErrors(User user, String repeatedPassword)  {
        UserValidator validator = new UserValidator(user, repeatedPassword);
        return validator.getErrors();
    }

    public User getByEmail(String email) throws SQLException, ClassNotFoundException {
        return dao.getByEmail(email);
    }

    public List<User> getAll() throws SQLException, ClassNotFoundException {
        return dao.getAllUsers();
    }

    public String getUserNameByEmail(String email) throws SQLException, ClassNotFoundException {
        return dao.getUserNameByEmail(email);
    }

    public boolean isAdmin(User user) {
        return user.getRole().getName().equalsIgnoreCase("admin");
    }

    public User getById(int id) throws SQLException, ClassNotFoundException {
        return dao.getById(id);
    }

    public int getUserIdByEmail(String email) throws SQLException, ClassNotFoundException {
       return dao.getByEmail(email).getId();
    }

    public void setUserIsBanned(int id, boolean isBanned) throws SQLException, ClassNotFoundException {
        dao.setUserIsBanned(id, isBanned);
    }

    public void updateUser(User user) throws SQLException, ClassNotFoundException {
        dao.edit(user);
    }

    public List<User> getCoAuthors(String email) throws SQLException, ClassNotFoundException {
        return dao.getCoAuthors(email);
    }
}
