package service;

import dao.impl.TagDAO;
import dao.interfaces.ITagDAO;
import model.Tag;

import java.sql.SQLException;
import java.util.List;

public class TagService {

    private ITagDAO tagDAO;

    public TagService() {
        this.tagDAO = TagDAO.getDao();
    }

    public List<Tag> getAllTags() throws SQLException, ClassNotFoundException {
        return tagDAO.getAllTags();
    }
}
