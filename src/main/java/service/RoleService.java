package service;

import dao.impl.RoleDAO;
import dao.interfaces.IRoleDAO;
import model.Role;

import java.sql.SQLException;
import java.util.List;

public class RoleService {

    private IRoleDAO roleDAO;

    public RoleService() {
        roleDAO = RoleDAO.getDao();
    }

    public List<Role> getAllRoles() throws SQLException, ClassNotFoundException {
        return roleDAO.getAll();
    }

    public Role getRoleById(int roleId) throws SQLException, ClassNotFoundException {
        return roleDAO.getRoleById(roleId);
    }
}
