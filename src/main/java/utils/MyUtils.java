package utils;

import javax.servlet.http.HttpSession;

public class MyUtils {

    public static void storeAuthorizedUserEmail(HttpSession session, String email) {
        session.setAttribute("authorizedUser", email);
    }

    public static String getAuthorizedUserEmail(HttpSession session) {
        return (String) session.getAttribute("authorizedUser");
    }
}
