package utils;

public class DBConfig {

    public static final String DRIVER_CLASS_NAME = "org.postgresql.Driver";
    public static final String JDBC_PROTOCOL = "jdbc:postgresql";
    public static final String HOST = "localhost";
    public static final String PORT = "5432";
    public static final String DB_NAME = "blog_s";
    public static final String USER = "postgres";
    public static final String PASS = "root";

    private DBConfig() {
    }
}
