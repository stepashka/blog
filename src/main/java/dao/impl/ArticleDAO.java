package dao.impl;

import dao.JDBCConnection;
import dao.interfaces.IArticleDAO;
import model.Article;
import model.Tag;
import model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class ArticleDAO implements IArticleDAO {

    private static ArticleDAO ArticleDAO;
    private UserDAO userDAO;
    private TagDAO tagDAO;

    private ArticleDAO() {
        userDAO = UserDAO.getDao();
        tagDAO = TagDAO.getDao();
    }

    public static ArticleDAO getArticleDAO() {
        if (ArticleDAO == null) {
            ArticleDAO = new ArticleDAO();
        }
        return ArticleDAO;
    }

    @Override
    public Article add(Article article) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("INSERT INTO article (title, image," +
                " short_description, content, co_authors, tags, author_id, published_date, creation_date, is_published) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);

        ps.setString(1, article.getTitle());
        ps.setString(2, article.getImage());
        ps.setString(3, article.getShortDescription());
        ps.setString(4, article.getContent());
        ps.setArray(5, JDBCConnection.getConnection().createArrayOf("integer", (article.getCoAuthors().isEmpty()) ? new Integer[0] : getArrayByCoAuthorList(article.getCoAuthors())));
        ps.setArray(6, JDBCConnection.getConnection().createArrayOf("integer", (article.getTags().isEmpty()) ? new Integer[0] : getArrayByTagList(article.getTags())));
        ps.setInt(7, article.getAuthor().getId());
        ps.setDate(8, Date.valueOf(article.getPublishedDate()));
        ps.setDate(9, Date.valueOf(article.getCreatedDate()));
        ps.setBoolean(10, article.isPublished());
        if (ps.executeUpdate() == 0) {
            throw new SQLException("Creation article failed!!!");
        }
        try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                article.setId(generatedKeys.getInt("id"));
            } else {
                throw new SQLException("Creating article failed, no id generated!");
            }
        }
        connection.close();
        return article;
    }

    public List<User> getCoAuthorsByIntegerList(List<Integer> integerCoAuthors) throws SQLException, ClassNotFoundException {
        List<User> coAuthors = new ArrayList<>();
        if (!integerCoAuthors.isEmpty()){
            for (int i : integerCoAuthors) {
                coAuthors.add(userDAO.getById(i));
            }
        }
        return coAuthors;
    }

    public List<Tag> getTagsByIntegerList(List<Integer> integerTags) throws SQLException, ClassNotFoundException {
        List<Tag> tags = new ArrayList<>();
        if (!integerTags.isEmpty()) {
            for (int i : integerTags) {
                tags.add(tagDAO.getTagById(i));
            }
        }
        return tags;
    }

    @Override
    public void delete(Article article) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("DELETE FROM article WHERE id = ?");
        ps.setInt(1, article.getId());
        ps.executeUpdate();
        connection.close();
    }

    private Integer[] getArrayByCoAuthorList(List<User> coAuthors) {
        Integer[] integers = null;
        if (!coAuthors.isEmpty()) {
            integers = new Integer[coAuthors.size()];
            for (int i = 0; i < coAuthors.size(); i++) {
                integers[i] = coAuthors.get(i).getId();
            }
        }
        return integers;
    }

    private Integer[] getArrayByTagList(List<Tag> tags) {
        Integer[] integers = null;
        if (!tags.isEmpty()) {
            integers = new Integer[tags.size()];
            for (int i = 0; i < tags.size(); i++) {
                integers[i] = tags.get(i).getId();
            }
        }
        return integers;
    }

    @Override
    public Article edit(Article article) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("UPDATE article SET title = ?, image = ?, " +
                "short_description = ?, content = ?, co_authors = ?, tags = ?, author_id = ?, " +
                "published_date = ?, creation_date = ?, is_published = ? WHERE id = ?");
        ps.setString(1, article.getTitle());
        if (article.getImage() != null) {
            ps.setString(2, article.getImage());
        }
        ps.setString(3, article.getShortDescription());
        ps.setString(4, article.getContent());
        ps.setArray(5, JDBCConnection.getConnection().createArrayOf("integer", (article.getCoAuthors().isEmpty()) ? new Integer[0] : getArrayByCoAuthorList(article.getCoAuthors())));
        ps.setArray(6, JDBCConnection.getConnection().createArrayOf("integer", (article.getTags().isEmpty()) ? new Integer[0] : getArrayByTagList(article.getTags())));
        if (article.getAuthor() == null) {
            ps.setNull(7, Types.INTEGER);
        } else {
            ps.setInt(7, article.getAuthor().getId());
        }
        ps.setDate(8, Date.valueOf(article.getPublishedDate()));
        ps.setDate(9, Date.valueOf(article.getCreatedDate()));
        ps.setBoolean(10, article.isPublished());
        ps.setInt(11, article.getId());

        ps.executeUpdate();
        connection.close();

        return getById(article.getId());
    }

    @Override
    public List<Article> getAllArticles() throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM article order by published_date DESC");
        ResultSet rs = ps.executeQuery();
        List<Article> articles = new ArrayList<>();
        while (rs.next()) {

            Article article = Article.newBuilder()
                    .setId(rs.getInt("id"))
                    .setTitle(rs.getString("title"))
                    .setImage(rs.getString("image"))
                    .setShortDescription(rs.getString("short_description"))
                    .setContent(rs.getString("content"))
                    .setCoAuthors(getCoAuthorsByIntegerList(asList((Integer[]) rs.getArray("co_authors").getArray())))
                    .setTags(getTagsByIntegerList(asList((Integer[]) rs.getArray("tags").getArray())))
                    .setAuthor(userDAO.getById(rs.getInt("author_id")))
                    .setPublishedDate(rs.getDate("published_date").toLocalDate())
                    .setCreatedDate(rs.getDate("creation_date").toLocalDate())
                    .setPublished(rs.getBoolean("is_published"))
                    .build();

            articles.add(article);
        }
        connection.close();
        return articles;
    }

    @Override
    public Article getById(int id) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM article WHERE id = ?");
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            Article article = Article.newBuilder()
                    .setId(rs.getInt("id"))
                    .setTitle(rs.getString("title"))
                    .setImage(rs.getString("image"))
                    .setShortDescription(rs.getString("short_description"))
                    .setContent(rs.getString("content"))
                    .setCoAuthors(getCoAuthorsByIntegerList(asList((Integer[]) rs.getArray("co_authors").getArray())))
                    .setTags(getTagsByIntegerList(asList((Integer[]) rs.getArray("tags").getArray())))
                    .setAuthor(userDAO.getById(rs.getInt("author_id")))
                    .setPublishedDate(rs.getDate("published_date").toLocalDate())
                    .setCreatedDate(rs.getDate("creation_date").toLocalDate())
                    .setPublished(rs.getBoolean("is_published"))
                    .build();
            connection.close();
            return article;
        }
        connection.close();
        return null;
    }

    @Override
    public List<Article> getArticlesByAuthorId(int authorId) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM article WHERE author_id = ? order by published_date DESC");
        ps.setInt(1, authorId);
        ResultSet rs = ps.executeQuery();
        List<Article> articles = new ArrayList<>();
        while (rs.next()) {

            Article article = Article.newBuilder()
                    .setId(rs.getInt("id"))
                    .setTitle(rs.getString("title"))
                    .setImage(rs.getString("image"))
                    .setShortDescription(rs.getString("short_description"))
                    .setContent(rs.getString("content"))
                    .setCoAuthors(getCoAuthorsByIntegerList(asList((Integer[]) rs.getArray("co_authors").getArray())))
                    .setTags(getTagsByIntegerList(asList((Integer[]) rs.getArray("tags").getArray())))
                    .setAuthor(userDAO.getById(rs.getInt("author_id")))
                    .setPublishedDate(rs.getDate("published_date").toLocalDate())
                    .setCreatedDate(rs.getDate("creation_date").toLocalDate())
                    .setPublished(rs.getBoolean("is_published"))
                    .build();

            articles.add(article);
        }
        connection.close();
        return articles;
    }

    @Override
    public List<Article> getArticlesByCoAuthorId(int coAuthorId) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM article WHERE co_authors @> ARRAY[?] order by published_date DESC");
        ps.setInt(1, coAuthorId);
        List<Article> articles = new ArrayList<>();
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Article article = Article.newBuilder()
                    .setId(rs.getInt("id"))
                    .setTitle(rs.getString("title"))
                    .setImage(rs.getString("image"))
                    .setShortDescription(rs.getString("short_description"))
                    .setContent(rs.getString("content"))
                    .setCoAuthors(getCoAuthorsByIntegerList(asList((Integer[]) rs.getArray("co_authors").getArray())))
                    .setTags(getTagsByIntegerList(asList((Integer[]) rs.getArray("tags").getArray())))
                    .setAuthor(userDAO.getById(rs.getInt("author_id")))
                    .setPublishedDate(rs.getDate("published_date").toLocalDate())
                    .setCreatedDate(rs.getDate("creation_date").toLocalDate())
                    .setPublished(rs.getBoolean("is_published"))
                    .build();

            articles.add(article);
        }
        connection.close();
        return articles;
    }

    @Override
    public List<Article> getArticlesByTagId(int tagId) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM article WHERE tags @> ARRAY[?] order by published_date DESC");
        ps.setInt(1, tagId);
        List<Article> articles = new ArrayList<>();
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Article article = Article.newBuilder()
                    .setId(rs.getInt("id"))
                    .setTitle(rs.getString("title"))
                    .setImage(rs.getString("image"))
                    .setShortDescription(rs.getString("short_description"))
                    .setContent(rs.getString("content"))
                    .setCoAuthors(getCoAuthorsByIntegerList(asList((Integer[]) rs.getArray("co_authors").getArray())))
                    .setTags(getTagsByIntegerList(asList((Integer[]) rs.getArray("tags").getArray())))
                    .setAuthor(userDAO.getById(rs.getInt("author_id")))
                    .setPublishedDate(rs.getDate("published_date").toLocalDate())
                    .setCreatedDate(rs.getDate("creation_date").toLocalDate())
                    .setPublished(rs.getBoolean("is_published"))
                    .build();

            articles.add(article);
        }
        connection.close();
        return articles;
    }

    @Override
    public List<Article> getNotPublishedArticlesByUserId(int userId) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection
                .prepareStatement("SELECT * FROM article WHERE article.is_published = false AND (article.author_id = ? OR article.co_authors @> ARRAY[?]) order by published_date DESC");
        ps.setInt(1, userId);
        ps.setInt(2, userId);
        List<Article> articles = new ArrayList<>();
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Article article = Article.newBuilder()
                    .setId(rs.getInt("id"))
                    .setTitle(rs.getString("title"))
                    .setImage(rs.getString("image"))
                    .setShortDescription(rs.getString("short_description"))
                    .setContent(rs.getString("content"))
                    .setCoAuthors(getCoAuthorsByIntegerList(asList((Integer[]) rs.getArray("co_authors").getArray())))
                    .setTags(getTagsByIntegerList(asList((Integer[]) rs.getArray("tags").getArray())))
                    .setAuthor(userDAO.getById(rs.getInt("author_id")))
                    .setPublishedDate(rs.getDate("published_date").toLocalDate())
                    .setCreatedDate(rs.getDate("creation_date").toLocalDate())
                    .setPublished(rs.getBoolean("is_published"))
                    .build();

            articles.add(article);
        }
        connection.close();
        return articles;
    }
}