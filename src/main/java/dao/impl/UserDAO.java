package dao.impl;

import dao.JDBCConnection;
import dao.interfaces.IUserDAO;
import model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO implements IUserDAO{

    private static UserDAO dao;
    private RoleDAO roleDAO;

    private UserDAO() {
        roleDAO = RoleDAO.getDao();
    }

    public static UserDAO getDao() {
        if (dao == null) {
            dao = new UserDAO();
        }
        return dao;
    }

    @Override
    public List<User> getAllUsers() throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        ArrayList<User> users = new ArrayList<>();
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM \"user\" order by surname ASC ");
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            User user = User.newBuilder()
                    .setId(rs.getInt("id"))
                    .setName(rs.getString("name"))
                    .setSurname(rs.getString("surname"))
                    .setEmail(rs.getString("email"))
                    .setPassword(rs.getString("password"))
                    .setRole(roleDAO.getRoleById(rs.getInt("role_id")))
                    .setBanned(rs.getBoolean("is_banned"))
                    .build();

            users.add(user);
        }
        connection.close();
        return users;
    }

    @Override
    public User add(User user) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("INSERT INTO \"user\" (name, surname, email, password, role_id, is_banned) VALUES " +
                "(?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);

        ps.setString(1, user.getName());
        ps.setString(2, user.getSurname());
        ps.setString(3, user.getEmail());
        ps.setString(4, user.getPassword());
        if (user.getRole() == null) {
            ps.setInt(5, 2);
        } else {
            ps.setInt(5, user.getRole().getId());
        }
        ps.setBoolean(6, user.isBanned());
        if (ps.executeUpdate() == 0) {
            throw new SQLException("Created user failed!!!");
        }

        try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                user.setId(generatedKeys.getInt(1));
            } else {
                throw new SQLException("Creating user failed, no id generated!");
            }
        }
        connection.close();
        return user;
    }

    @Override
    public void delete(User user) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("DELETE FROM \"user\" WHERE id = ?");
        ps.setInt(1, user.getId());
        ps.executeUpdate();
        connection.close();
    }

    @Override
    public User edit(User user) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("UPDATE \"user\" SET name = ?, surname = ?, email = ?," +
                " password = ?, role_id = ? WHERE id = ?");

        ps.setString(1, user.getName());
        ps.setString(2, user.getSurname());
        ps.setString(3, user.getEmail());
        ps.setString(4, user.getPassword());
        ps.setInt(5, user.getRole().getId());
        ps.setInt(6, user.getId());

        ps.executeUpdate();
        connection.close();

        return getById(user.getId());
    }

    @Override
    public User getById(int id) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM \"user\" WHERE id = ?");
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            User user = User.newBuilder()
                    .setId(rs.getInt("id"))
                    .setName(rs.getString("name"))
                    .setSurname(rs.getString("surname"))
                    .setEmail(rs.getString("email"))
                    .setPassword(rs.getString("password"))
                    .setRole(roleDAO.getRoleById(rs.getInt("role_id")))
                    .setBanned(rs.getBoolean("is_banned"))
                    .build();
            connection.close();
            return user;
        }
        connection.close();
        return null;
    }

    public List<User> getCoAuthors(String email) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM \"user\" WHERE email NOT IN (?) order by surname ASC ");
        ps.setString(1, email);
        ResultSet rs = ps.executeQuery();
        List<User> coAuthors = new ArrayList<>();

        while (rs.next()) {
            User user = User.newBuilder()
                    .setId(rs.getInt("id"))
                    .setName(rs.getString("name"))
                    .setSurname(rs.getString("surname"))
                    .setEmail(rs.getString("email"))
                    .setPassword(rs.getString("password"))
                    .setRole(roleDAO.getRoleById(rs.getInt("role_id")))
                    .setBanned(rs.getBoolean("is_banned"))
                    .build();
            coAuthors.add(user);
        }
        connection.close();
        return coAuthors;
    }

    public User getByEmail(String email) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM \"user\" WHERE email = ?");
        ps.setString(1, email);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            User user = User.newBuilder()
                    .setId(rs.getInt("id"))
                    .setName(rs.getString("name"))
                    .setSurname(rs.getString("surname"))
                    .setEmail(rs.getString("email"))
                    .setPassword(rs.getString("password"))
                    .setRole(roleDAO.getRoleById(rs.getInt("role_id")))
                    .setBanned(rs.getBoolean("is_banned"))
                    .build();
            connection.close();
            return user;
        }
        connection.close();
        return null;
    }

    @Override
    public String getUserNameByEmail(String email) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("SELECT name FROM \"user\" WHERE email = ?");
        ps.setString(1, email);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            String name = rs.getString("name");
            connection.close();
            return name;
        }
        connection.close();
        return null;
    }

    @Override
    public void setUserIsBanned(int id, boolean isBanned) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("UPDATE \"user\" SET is_banned = ? WHERE id = ?");
        ps.setBoolean(1, isBanned);
        ps.setInt(2, id);
        ps.executeUpdate();
        connection.close();
    }
}