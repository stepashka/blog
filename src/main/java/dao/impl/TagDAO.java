package dao.impl;

import dao.JDBCConnection;
import dao.interfaces.ITagDAO;
import model.Tag;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TagDAO implements ITagDAO {

    private static TagDAO dao;

    private TagDAO() {}

    public static TagDAO getDao() {
        if (dao == null) {
            dao = new TagDAO();
        }
        return dao;
    }

    @Override
    public Tag getTagById(int id) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM tag WHERE id = ?");
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();

        if (rs.next()) {
            Tag tag = Tag.newBuilder()
                    .setId(rs.getInt("id"))
                    .setName(rs.getString("name"))
                    .build();
            connection.close();
            return tag;
        }
        connection.close();
        return null;
    }

    @Override
    public List<Tag> getAllTags() throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        List<Tag> tags = new ArrayList<>();
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM tag");
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            Tag tag = Tag.newBuilder()
                    .setId(rs.getInt("id"))
                    .setName(rs.getString("name"))
                    .build();
            tags.add(tag);
        }
        connection.close();
        return tags;
    }

    @Override
    public Tag add(Tag tag) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("INSERT INTO tag (name) VALUES (?)",
                Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, tag.getName());

        if (ps.executeUpdate() == 0) {
            throw new SQLException("Creation tag failed!");
        }

        try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                tag.setId(generatedKeys.getInt(1));
            } else {
                throw new SQLException("Creating tag failed, no id generated!");
            }
        }
        connection.close();
        return tag;
    }

    @Override
    public Tag updateTag(Tag tag) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("UPDATE tag SET name = ? WHERE id = ?");

        ps.setString(1, tag.getName());
        ps.setInt(2, tag.getId());

        ps.executeUpdate();
        connection.close();
        return getTagById(tag.getId());
    }

    @Override
    public void deleteTag(Tag tag) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("DELETE FROM tag WHERE id = ?");
        ps.setInt(1, tag.getId());

        ps.executeUpdate();
        connection.close();
    }
}
