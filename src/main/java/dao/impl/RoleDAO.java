package dao.impl;

import dao.JDBCConnection;
import dao.interfaces.IRoleDAO;
import model.Role;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RoleDAO implements IRoleDAO {

    private static RoleDAO dao;

    private RoleDAO() {}

    public static RoleDAO getDao() {
        if (dao == null) {
            dao = new RoleDAO();
        }
        return dao;
    }

    @Override
    public Role add(Role role) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("INSERT INTO role (name) VALUES (?)",
                Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, role.getName());
        if (ps.executeUpdate() == 0) {
            throw new SQLException("Creation role failed");
        }
        try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                role.setId(generatedKeys.getInt(1));
            } else {
                throw new SQLException("Creating role failed, no id generated");
            }
        }

        connection.close();
        return role;
    }

    @Override
    public void delete(Role role) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("DELETE FROM role WHERE id = ?");
        ps.setInt(1, role.getId());
        ps.executeUpdate();
        connection.close();
    }

    @Override
    public Role edit(Role role) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("UPDATE role SET name = ? WHERE id = ?");
        ps.setString(1, role.getName());
        ps.setInt(2, role.getId());

        ps.executeUpdate();
        connection.close();
        return getRoleById(role.getId());
    }

    @Override
    public List<Role> getAll() throws SQLException, ClassNotFoundException {
        List<Role> roles = new ArrayList<>();
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM role");
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            Role role = new Role();
            role.setId(rs.getInt("id"));
            role.setName(rs.getString("name"));
            roles.add(role);
        }
        connection.close();
        return roles;
    }

    @Override
    public Role getRoleById(int id) throws SQLException, ClassNotFoundException {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM role WHERE id = ?");
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();

        if (rs.next()) {
            Role role = new Role();
            role.setId(rs.getInt("id"));
            role.setName(rs.getString("name"));
            connection.close();
            return role;
        }
        connection.close();
        return null;
    }

}
