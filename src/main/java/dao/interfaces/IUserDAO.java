package dao.interfaces;

import model.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserDAO {

    User add(User user) throws SQLException, ClassNotFoundException;
    void delete(User user) throws SQLException, ClassNotFoundException;
    User edit(User user) throws SQLException, ClassNotFoundException;
    List<User> getAllUsers() throws SQLException, ClassNotFoundException;
    User getById(int id) throws SQLException, ClassNotFoundException;
    User getByEmail(String email) throws SQLException, ClassNotFoundException;
    String getUserNameByEmail(String email) throws SQLException, ClassNotFoundException;
    void setUserIsBanned(int id, boolean isBanned) throws SQLException, ClassNotFoundException;
    List<User> getCoAuthors(String email) throws SQLException, ClassNotFoundException;

}
