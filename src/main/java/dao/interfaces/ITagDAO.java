package dao.interfaces;

import model.Tag;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface ITagDAO {

    List<Tag> getAllTags() throws SQLException, ClassNotFoundException;
    Tag getTagById(int id) throws SQLException, ClassNotFoundException;
    Tag add(Tag tag) throws SQLException, ClassNotFoundException;
    Tag updateTag(Tag tag) throws SQLException, IOException, ClassNotFoundException;
    void deleteTag(Tag tag) throws SQLException, ClassNotFoundException;

}
