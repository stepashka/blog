package dao.interfaces;

import model.Role;

import java.sql.SQLException;
import java.util.List;

public interface IRoleDAO {

    Role add(Role role) throws SQLException, ClassNotFoundException;
    void delete(Role role) throws SQLException, ClassNotFoundException;
    Role edit(Role role) throws SQLException, ClassNotFoundException;
    List<Role> getAll() throws SQLException, ClassNotFoundException;
    Role getRoleById(int id) throws SQLException, ClassNotFoundException;

}
