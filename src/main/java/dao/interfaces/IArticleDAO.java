package dao.interfaces;

import model.Article;
import model.Tag;
import model.User;

import java.sql.SQLException;
import java.util.List;

public interface IArticleDAO {

    Article add(Article article) throws SQLException, ClassNotFoundException;
    void delete(Article article) throws SQLException, ClassNotFoundException;
    Article edit(Article article) throws SQLException, ClassNotFoundException;
    List<Article> getAllArticles() throws SQLException, ClassNotFoundException;
    Article getById(int id) throws SQLException, ClassNotFoundException;
    List<Article> getArticlesByAuthorId(int authorId) throws SQLException, ClassNotFoundException;
    List<Article> getNotPublishedArticlesByUserId(int userId) throws SQLException, ClassNotFoundException;
    List<Article> getArticlesByTagId(int tagId) throws SQLException, ClassNotFoundException;
    List<Article> getArticlesByCoAuthorId(int coAuthorId) throws SQLException, ClassNotFoundException;
    List<Tag> getTagsByIntegerList(List<Integer> integerTags) throws SQLException, ClassNotFoundException;
    List<User> getCoAuthorsByIntegerList(List<Integer> integerCoAuthors) throws SQLException, ClassNotFoundException;

}
