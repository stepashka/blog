package dao;

import utils.DBConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {

    private JDBCConnection() {
    }

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(DBConfig.DRIVER_CLASS_NAME);

        Connection connection = DriverManager.getConnection(DBConfig.JDBC_PROTOCOL + "://" + DBConfig.HOST + ":" +
                DBConfig.PORT + "/" + DBConfig.DB_NAME, DBConfig.USER, DBConfig.PASS);

        return connection;
    }
}
