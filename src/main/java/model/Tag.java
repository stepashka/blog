package model;

import java.util.Objects;

public class Tag {

    private int id;
    private String name;

    public Tag() {
    }

    public Tag(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Builder newBuilder() {
        return new Tag().new Builder();
    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return id == tag.id &&
                Objects.equals(name, tag.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }

    public class Builder {

        private Builder() {

        }

        public Builder setId(int id) {
            Tag.this.id = id;

            return this;
        }

        public Builder setName(String name) {
            Tag.this.name = name;

            return this;
        }

        public Tag build() {
            return Tag.this;
        }
    }
}
