package model;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class Article {

    private int id;
    private String title;
    private String image;
    private String shortDescription;
    private String content;
    private List<User> coAuthors;
    private List<Tag> tags;
    private User author;
    private LocalDate publishedDate;
    private LocalDate createdDate;
    private boolean isPublished;

    public Article() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDate getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(LocalDate publishedDate) {
        this.publishedDate = publishedDate;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isPublished() {
        return isPublished;
    }

    public void setPublished(boolean published) {
        isPublished = published;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public List<User> getCoAuthors() {
        return coAuthors;
    }

    public void setCoAuthors(List<User> coAuthors) {
        this.coAuthors = coAuthors;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public static Builder newBuilder() {
        return new Article().new Builder();
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", shortDescription='" + shortDescription + '\'' +
                ", content='" + content + '\'' +
                ", coAuthors=" + coAuthors +
                ", tags=" + tags +
                ", author=" + author +
                ", publishedDate=" + publishedDate +
                ", createdDate=" + createdDate +
                ", isPublished=" + isPublished +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Article article = (Article) o;
        return id == article.id &&
                isPublished == article.isPublished &&
                Objects.equals(title, article.title) &&
                Objects.equals(image, article.image) &&
                Objects.equals(shortDescription, article.shortDescription) &&
                Objects.equals(content, article.content) &&
                Objects.equals(coAuthors, article.coAuthors) &&
                Objects.equals(tags, article.tags) &&
                Objects.equals(author, article.author) &&
                Objects.equals(publishedDate, article.publishedDate) &&
                Objects.equals(createdDate, article.createdDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, title, image, shortDescription, content, coAuthors, tags, author, publishedDate, createdDate, isPublished);
    }

    public class Builder {

        private Builder(){}

        public Builder setId(int id) {
            Article.this.id = id;

            return this;
        }

        public Builder setTitle(String title) {
            Article.this.title = title;

            return this;
        }

        public Builder setImage(String image) {
            Article.this.image = image;

            return this;
        }

        public Builder setShortDescription(String shortDescription) {
            Article.this.shortDescription = shortDescription;

            return this;
        }

        public Builder setContent(String content) {
            Article.this.content = content;

            return this;
        }

        public Builder setCoAuthors(List<User> coAuthors) {
            Article.this.coAuthors = coAuthors;

            return this;
        }

        public Builder setTags(List<Tag> tags) {
            Article.this.tags = tags;

            return this;
        }

        public Builder setAuthor(User author) {
            Article.this.author = author;

            return this;
        }

        public Builder setPublishedDate(LocalDate publishedDate) {
            Article.this.publishedDate = publishedDate;

            return this;
        }

        public Builder setCreatedDate(LocalDate createdDate) {
            Article.this.createdDate = createdDate;

            return this;
        }

        public Builder setPublished(boolean published) {
            Article.this.isPublished = published;

            return this;
        }

        public Article build() {
            return Article.this;
        }
    }
}
