package model;


import javax.validation.constraints.*;
import java.util.Objects;

public class User {

    private int id;

    @NotEmpty(message = "is required")
    private String name;

    @NotEmpty(message = "is required")
    private String surname;

    @NotEmpty(message = "is required")
    @Email
    private String email;

    //^                 # start-of-string
    //(?=.*[0-9])       # a digit must occur at least once
    //(?=.*[a-z])       # a lower case letter must occur at least once
    //(?=.*[A-Z])       # an upper case letter must occur at least once
    //(?=\S+$)          # no whitespace allowed in the entire string
    //.{8,}             # anything, at least eight places though
    //$                 # end-of-string
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$", message = " must contain at least one digit, " +
            "lower case letter, upper case letter, no whitespace, at least 8 symbols")
    private String password;
    private Role role;
    private boolean isBanned;

    public User() {
    }

    public User(@NotEmpty String name, @NotEmpty String surname, @NotEmpty @Email String email, @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$", message = "Неправльно введено пароль") String password, Role role) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public User(@NotEmpty String name, @NotEmpty String surname, @NotEmpty @Email String email, @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$", message = "Неправльно введено пароль") String password) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isBanned() {
        return isBanned;
    }

    public void setBanned(boolean banned) {
        isBanned = banned;
    }

    public static Builder newBuilder() {
        return new User().new Builder();
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                isBanned == user.isBanned &&
                Objects.equals(name, user.name) &&
                Objects.equals(surname, user.surname) &&
                Objects.equals(email, user.email) &&
                Objects.equals(password, user.password) &&
                Objects.equals(role, user.role);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, surname, email, password, role, isBanned);
    }

    public class Builder {

        private Builder() {}

        public Builder setId(int id) {
            User.this.id = id;

            return this;
        }

        public Builder setName(String name) {
            User.this.name = name;

            return this;
        }

        public Builder setSurname(String surname) {
            User.this.surname = surname;

            return this;
        }

        public Builder setEmail(String email) {
            User.this.email = email;

            return this;
        }

        public Builder setPassword(String password) {
            User.this.password = password;

            return this;
        }

        public Builder setRole(Role role) {
            User.this.role = role;

            return this;
        }

        public Builder setBanned(boolean banned) {
            User.this.isBanned = banned;

            return this;
        }

        public User build() {
            return User.this;
        }
    }
}
