package servlet;

import model.Article;
import org.apache.log4j.Logger;
import service.ArticleService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = {"/publish"})
public class PublishArticle extends HttpServlet {

    private final static Logger LOGGER = Logger.getLogger(PublishArticle.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("isPublished") != null) {
            ArticleService articleService = new ArticleService();
            try {
                Article article = articleService.getArticleById(Integer.parseInt(req.getParameter("id")));
                article.setPublished(Boolean.parseBoolean(req.getParameter("isPublished")));
                articleService.updateArticle(article);
                resp.sendRedirect("home");
            } catch (SQLException | ClassNotFoundException e) {
                LOGGER.error(e.getMessage());
                resp.sendRedirect("exception.jsp");
            }
        }
    }
}
