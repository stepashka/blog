package servlet;

import model.User;
import org.apache.log4j.Logger;
import service.RoleService;
import service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = {"/userList"})
@MultipartConfig
public class UserList extends HttpServlet {

    private final static Logger LOGGER = Logger.getLogger(ArticleServlet.class);
    private UserService userService = new UserService();
    private RoleService roleService = new RoleService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            req.setAttribute("roles", roleService.getAllRoles());
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error("Something wrong", e);
            resp.sendRedirect("exception.jsp");
            return;
        }
        if (req.getParameter("id") != null) {
            int id = Integer.parseInt(req.getParameter("id"));
            boolean isBanned = Boolean.parseBoolean(req.getParameter("isBanned"));
            try {
                userService.setUserIsBanned(id, isBanned);
            } catch (SQLException | ClassNotFoundException e) {
                LOGGER.error("Something wrong", e);
                resp.sendRedirect("exception.jsp");
                return;
            }
        }

        try {
            req.setAttribute("userList", userService.getAllUsers());
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error("Something wrong", e);
            resp.sendRedirect("exception.jsp");
            return;
        }
        RequestDispatcher dispatcher = req.getRequestDispatcher("userList.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("roleId") != null && req.getParameter("uid") != null) {
            int roleId = Integer.parseInt(req.getParameter("roleId"));
            int userId = Integer.parseInt(req.getParameter("uid"));
            try {
                User user = userService.getById(userId);
                user.setRole(roleService.getRoleById(roleId));
                userService.updateUser(user);
            } catch (SQLException | ClassNotFoundException e) {
                LOGGER.error("Something wrong", e);
                resp.sendRedirect("exception");
                return;
            }
        }
        doGet(req, resp);
    }
}
