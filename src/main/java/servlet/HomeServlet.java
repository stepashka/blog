package servlet;

import model.Article;
import org.apache.log4j.Logger;
import service.ArticleService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/home")
public class HomeServlet extends HttpServlet {

    private final static Logger LOGGER = Logger.getLogger(ArticleServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArticleService articleService = new ArticleService();
        List<Article> articles;
            try {
                articles = articleService.getAllArticles();
            } catch (SQLException | ClassNotFoundException e) {
                LOGGER.error(e.getMessage());
                resp.sendRedirect("exception");
                return;
            }
        req.setAttribute("articleList", articles);
        RequestDispatcher dispatcher = req.getRequestDispatcher("index.jsp");
        dispatcher.forward(req, resp);
    }
}
