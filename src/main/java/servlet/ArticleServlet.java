package servlet;

import model.Article;
import model.User;
import org.apache.log4j.Logger;
import service.ArticleService;
import service.UserService;
import utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = {"/article"})
public class ArticleServlet extends HttpServlet {

    private final static Logger LOGGER = Logger.getLogger(ArticleServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        ArticleService service = new ArticleService();
        Article article;
        try {
            article = service.getArticleById(id);
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error(e.getMessage());
            resp.sendRedirect("exception");
            return;
        }
        if (article == null) {
            resp.sendRedirect("home");
            return;
        }
        UserService userService = new UserService();
        User user;
        try {
            user = userService.getByEmail(MyUtils.getAuthorizedUserEmail(req.getSession()));
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error(e.getMessage());
            resp.sendRedirect("exception");
            return;
        }
        boolean isCoAuthor = article.getCoAuthors().contains(user);
        req.setAttribute("isCoAuthor", isCoAuthor);
        req.setAttribute("article", article);
        RequestDispatcher rs = req.getServletContext().getRequestDispatcher("/article.jsp");
        rs.forward(req, resp);
    }
}