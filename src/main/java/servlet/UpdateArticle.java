package servlet;

import model.Article;
import org.apache.log4j.Logger;
import service.ArticleService;
import service.TagService;
import service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@WebServlet(urlPatterns = {"/updateArticle"})
@MultipartConfig
public class UpdateArticle extends HttpServlet {

    private final static Logger LOGGER = Logger.getLogger(ArticleServlet.class);
    private int id;
    private ArticleService articleService = new ArticleService();
    private TagService tagService = new TagService();
    private UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        id = Integer.parseInt(req.getParameter("id"));
        try {
            Article article = articleService.getArticleById(id);
            req.setAttribute("editingArticle", article);
            req.setAttribute("users", userService.getCoAuthors(article.getAuthor().getEmail()));
            req.setAttribute("tags", tagService.getAllTags());
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error(e.getMessage());
            resp.sendRedirect("exception");
            return;
        }

        RequestDispatcher dispatcher = req.getRequestDispatcher("updateArticle.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Article article;
        try {
            article = articleService.getArticleById(id);
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error(e.getMessage());
            resp.sendRedirect("exception");
            return;
        }

        String toDB = articleService.getImagePath(req.getPart("file"), req.getServletContext().getRealPath(""));
        toDB = toDB == null ? article.getImage() : toDB;

        List<Integer> coAuthors = new ArrayList<>();
        if (req.getParameterValues("chooseCoAuthors") != null) {
            for (String i : req.getParameterValues("chooseCoAuthors")) {
                coAuthors.add(Integer.valueOf(i));
            }
        }
        List<Integer> tags = new ArrayList<>();
        if (req.getParameterValues("chooseTags") != null) {
            for (String i : req.getParameterValues("chooseTags")) {
                tags.add(Integer.valueOf(i));
            }
        }
        String title = req.getParameter("title");
        String shortDescription = req.getParameter("shortDescription");
        String content = req.getParameter("content");
        boolean publish = req.getParameter("publish") == null ? article.isPublished() : req.getParameter("publish") != null;
        try {
            article = Article.newBuilder()
                    .setId(article.getId())
                    .setTitle(title)
                    .setImage(toDB)
                    .setShortDescription(shortDescription)
                    .setContent(content)
                    .setCoAuthors(articleService.getCoAuthorsByIntegerList(coAuthors))
                    .setTags(articleService.getTagsByIntegerList(tags))
                    .setAuthor(article.getAuthor())
                    .setPublishedDate(LocalDate.now())
                    .setCreatedDate(article.getCreatedDate())
                    .setPublished(publish)
                    .build();
            articleService.updateArticle(article);
            resp.sendRedirect("home");
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error(e.getMessage());
            resp.sendRedirect("exception");
        }
    }
}