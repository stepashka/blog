package servlet;

import model.User;
import org.apache.log4j.Logger;
import service.UserService;
import utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

@WebServlet("/login")
public class Login extends HttpServlet {

        private final static Logger LOGGER = Logger.getLogger(ArticleServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("login.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        UserService userService = new UserService();
        try {
            if (userService.checkUser(email, password)) {
                User user = userService.getByEmail(email);
                if (user.isBanned()) {
                    resp.sendRedirect("userBannedPage.jsp");
                } else {
                    MyUtils.storeAuthorizedUserEmail(req.getSession(), email);
                    req.getSession().setAttribute("authorizedUserName", userService.getUserNameByEmail(email));
                    req.getSession().setAttribute("isAdmin", userService.isAdmin(userService.getByEmail(email)));
                    resp.sendRedirect("home");
                }
            } else {
                String errorMessage = "Email or password is incorrect";

                req.setAttribute("errorMessage", errorMessage);

               doGet(req, resp);
            }
        } catch (NoSuchAlgorithmException | SQLException | ClassNotFoundException e) {
            LOGGER.error(e.getMessage());
            resp.sendRedirect("exception");
        }
    }
}