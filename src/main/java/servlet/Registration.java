package servlet;

import model.User;
import org.apache.log4j.Logger;
import service.UserService;
import utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;


@WebServlet("/registration")
public class Registration extends HttpServlet {

    private final static Logger LOGGER = Logger.getLogger(ArticleServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");

        String repeatedPassword = req.getParameter("repeatedPassword");

        UserService service = new UserService();

        User user = User.newBuilder()
                .setName(req.getParameter("name"))
                .setSurname(req.getParameter("surname"))
                .setEmail(req.getParameter("email"))
                .setPassword(req.getParameter("password"))
                .setRole(null)
                .build();

        if (!service.getErrors(user, repeatedPassword).isEmpty()) {
            req.setAttribute("errors", service.getErrors(user, repeatedPassword));
            doGet(req, resp);
            return;
        }
        try {
            service.writeToDB(user);
        } catch (SQLException | ClassNotFoundException | NoSuchAlgorithmException e) {
            LOGGER.error(e.getMessage());
            resp.sendRedirect("exception");
            return;
        }
        HttpSession session = req.getSession();
        MyUtils.storeAuthorizedUserEmail(session, user.getEmail());
        req.getSession().setAttribute("authorizedUserName", user.getName());
        resp.sendRedirect("home");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rs = req.getRequestDispatcher("registration.jsp");
        rs.forward(req, resp);
    }
}
