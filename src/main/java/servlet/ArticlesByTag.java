package servlet;

import org.apache.log4j.Logger;
import service.ArticleService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = {"/articlesByTag"})
public class ArticlesByTag extends HttpServlet {

    private final static Logger LOGGER = Logger.getLogger(ArticleServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArticleService articleService = new ArticleService();
        if (req.getParameter("id") != null) {
            try {
                req.setAttribute("articleList", articleService.getArticleByTagId(Integer.parseInt(req.getParameter("id"))));
            } catch (SQLException | ClassNotFoundException e) {
                LOGGER.error("Something wrong", e);
                LOGGER.error(e.getMessage());
                return;
            }
        }
        RequestDispatcher dispatcher = req.getRequestDispatcher("/myPosts.jsp");
        dispatcher.forward(req, resp);
    }
}
