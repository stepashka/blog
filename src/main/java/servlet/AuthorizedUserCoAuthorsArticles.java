package servlet;

import org.apache.log4j.Logger;
import service.ArticleService;
import service.UserService;
import utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = {"/myCoAuthorPosts"})
public class AuthorizedUserCoAuthorsArticles extends HttpServlet {

    private final static Logger LOGGER = Logger.getLogger(AuthorizedUserCoAuthorsArticles.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArticleService articleService = new ArticleService();
        UserService userService = new UserService();
        try {
            req.setAttribute("articleList", articleService
                    .getArticlesByCoAuthorId(userService
                            .getUserIdByEmail(MyUtils.getAuthorizedUserEmail(req.getSession()))));
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error(e.getMessage());
            resp.sendRedirect("exception");
            return;
        }
        RequestDispatcher dispatcher = req.getRequestDispatcher("myCoAuthorPosts.jsp");
        dispatcher.forward(req, resp);
    }
}