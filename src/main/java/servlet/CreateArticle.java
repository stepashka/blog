package servlet;

import model.Article;
import org.apache.log4j.Logger;
import service.ArticleService;
import service.TagService;
import service.UserService;
import utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = {"/createArticle"})
@MultipartConfig
public class CreateArticle extends HttpServlet {

    private ArticleService articleService = new ArticleService();
    private TagService tagService = new TagService();
    private UserService userService = new UserService();
    private final static Logger LOGGER = Logger.getLogger(CreateArticle.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

        try {
            req.setAttribute("users", userService.getCoAuthors(MyUtils.getAuthorizedUserEmail(session)));
            req.setAttribute("tags", tagService.getAllTags());
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error(e.getMessage());
            resp.sendRedirect("exception");
            return;
        }
        RequestDispatcher dispatcher = req.getRequestDispatcher("createArticle.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String toDB = articleService.getImagePath(req.getPart("file"), req.getServletContext().getRealPath(""));
        toDB = toDB == null ? "img/about-bg.jpg" : toDB;
        List<Integer> coAuthors = new ArrayList<>();
        if (req.getParameterValues("chooseCoAuthors") != null) {
            for (String i : req.getParameterValues("chooseCoAuthors")) {
                coAuthors.add(Integer.valueOf(i));
            }
        }
        List<Integer> tags = new ArrayList<>();
        if (req.getParameterValues("chooseTags") != null) {
            for (String i : req.getParameterValues("chooseTags")) {
                tags.add(Integer.valueOf(i));
            }
        }
        boolean publish = req.getParameter("publish") != null;
        try {
            Article article = Article.newBuilder()
                    .setTitle(req.getParameter("title"))
                    .setImage(toDB)
                    .setShortDescription(req.getParameter("shortDescription"))
                    .setContent(req.getParameter("content"))
                    .setCoAuthors(articleService.getCoAuthorsByIntegerList(coAuthors))
                    .setTags(articleService.getTagsByIntegerList(tags))
                    .setAuthor(userService.getByEmail(MyUtils.getAuthorizedUserEmail(req.getSession())))
                    .setPublishedDate(LocalDate.now())
                    .setCreatedDate(LocalDate.now())
                    .setPublished(publish)
                    .build();
            resp.sendRedirect("article?id=" + articleService.createArticle(article).getId());
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error(e.getMessage());
            resp.sendRedirect("exception");
        }
    }
}
