package servlet;

import model.User;
import org.apache.log4j.Logger;
import service.UserService;
import utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = { "/userInfo" })
public class UserInfo extends HttpServlet {

    private final static Logger LOGGER = Logger.getLogger(ArticleServlet.class.getName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserService userService = new UserService();

        try {
            User user = userService.getByEmail(MyUtils.getAuthorizedUserEmail(req.getSession()));
            req.setAttribute("user", user);
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error(e.getMessage());
            resp.sendRedirect("exception");
            return;
        }
        RequestDispatcher dispatcher = req.getRequestDispatcher("/userInfo.jsp");
        dispatcher.forward(req, resp);
    }
}
