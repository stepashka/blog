package servlet;

import org.apache.log4j.Logger;
import service.ArticleService;
import service.UserService;
import utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/drafts")
public class Drafts extends HttpServlet {

    private final static Logger LOGGER = Logger.getLogger(ArticleServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArticleService articleService = new ArticleService();
        UserService userService = new UserService();
        try {
            req.setAttribute("articleList", articleService
                    .getNotPublishedArticlesByUserId(userService
                            .getUserIdByEmail(MyUtils.getAuthorizedUserEmail(req.getSession()))));
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error(e.getMessage());
            resp.sendRedirect("exception");
            return;
        }
        RequestDispatcher dispatcher = req.getRequestDispatcher("drafts.jsp");
        dispatcher.forward(req, resp);
    }
}